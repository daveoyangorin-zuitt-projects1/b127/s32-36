const express = require('express');
const router = express.Router();
const productsController = require ('../controllers/productsControllers');
const auth = require('../auth');


router.post('/', auth.verify, (req,res) =>{
	const data = {
		course:req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.addCourse(data).then(result => res.send(result));

})

//retrieve all products
router.get('/all', (req,res) => {
	courseController.getAllCourses().then(result => res.send(result));
})

//retrieve all True courses
router.get('/', (req,res) => {
	courseController.getAllActive().then(result => res.send(result))
})

//Retrieve Specific course
router.get('/:courseId', (req,res) => {
	courseController.specificCourse(req.params).then(result => res.send(result))
})




//update a course
router.put('/:courseId',auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		courseController.updateCourse(req.params, req.body).then(result => res.send(result))
	}else{
		res.send(false)
	}
})



//archive a course/soft delete a course
router.put('/:courseId/archive', auth.verify, (req, res) => {
	isAdmin: auth.decode(reqParams.courseId, updateCourse).then((course , error))
	courseController.archiveCourse(req.params).then(result => res.send(result)) 
})

module.exports = router;

