const mongoose = require('mongoose');


const productsSchema = new mongoose.Schema({
	name: {
		type: String,
		required : [true, "Course is required"]
	},
	price : {
		type: Number,
		required : [true, 'Price is required']
	},
	description : {
		type: String,
		required : [true, 'Description is required']
	},
	isActive: {
		type: Boolean,
		default: true,
	},
	createdOn : {
		type: Date,
		default: new Date()
	},
	category: {
		type: String,
		required : [true, 'Category is required']
	},
	stock: {
		type: String,
		required : [true, 'Stock is required']
	}
})

module.exports = mongoose.model("Products", productsSchema);




