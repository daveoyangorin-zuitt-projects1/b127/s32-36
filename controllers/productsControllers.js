const products = require('../models/products');

module.exports.addProduct = (data) => {
	//User is an admin
	if(data.isAdmin){
		let newProduct = new product({
			name : data.product.name,
			price: data.product.price,
			description: data.course.description,
			category: data.product.category,
			stock: data.product.stock,
			price: data.course.price
		})
		return newCourse.save().then((course, error) =>{
			//Course creation is failed
			if(error){
				return false;
			}else{
				message: 'added product';
			}
		})
	}else{
		//user is not an admin
		return false;
	}
}

//Retreive all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

//retrieve all true courses
module.exports.getAllActive = () => {
	return Course.find({isActive : true}).then(result => {
		return result;
	})
}

//retrieve specific courses
module.exports.specificCourse = (reqparams) => {
	return Course.findById(reqparams.courseId).then(result => {
		return result;
	}) 
	
}

/*//update a course

module.exports.updateCourse = (req) => {
    return Course.findByIdAndUpdate({ 
    		_id: req.params.courseId }, 
        {   name: req.body.name, 
            description: req.body.description, 
            price: req.body.price 
        })
    .then(updatedCourse => { 
        return (updatedCourse) 
            ? { message: "Course update was successful", data:updatedCourse} 
            : { message: "Course update failed" }; })
    .catch(error => res.status(500).send({message: "Internal Server Error"}))
}
*/


module.exports.updateCourse = (reqParams, reqBody) => {
	let updateCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

//archive a course

module.exports.archiveCourse = (reqParams) => {
	let updatedCourse = {
		isActive: false
	}
	return course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}
