const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const Course = require('../models/Course')


//Check if the email already exists
module.exports.checkEmailExists = (reqBody) => {
	return User.find( { email: reqBody.email } ).then(result => {
		//The "find" mthod returns a record if a match is found
		if(result.length > 0){
			return true;
		}else{
			//No duplicate email found
			//The user is not yet registered in the database
			return false;
		}
	})
}


module.exports.registerUser = (reqBody) =>{

//Register New User
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		shippingAddress: reqBody.shippingAddress,
		mobileNo: reqBody.mobileNo,
		country: reqBody.country,
		password: bcrypt.hashSync(reqBody.password, 10)
	})


	return newUser.save().then((user, error) => {
		if(error){
			return console.log("Something missing a field");
		}else{
			return console.log('Successfuly register');
		}
	})
}


module.exports.loginUser = (reqBody) => {
	return User.findOne( {email: reqBody.email} ).then(result => {
		//If user does not exist
		if(result == null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect){
				return { accessToken: auth.createAccessToken(result.toObject()) }
			}else{
				return false;
			}
		}
	})
}


module.exports.getProfile = (data) => {
	return User.findById( data.userId ).then(result => {
		 result.password = "";
		
		return result;
	})
}


module.exports.enroll = async (data) => {
	//create isUserUpdate
	let isUserUpdated = await User.findById(data.userId).then(user =>{
		user.enrollments.push({ courseId: data.courseId })
	
	return user.save().then((user, error) => {
		if(error) {
			return false;
		}else{
			return true;
		}
	})
   })



	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		//add the userId in the course enrollees array
		course.enrollees.push({ userId: data.userId });

		return course.save().then((course, error) =>{
			if(error){
				return false;
			}else{
				return true;
			}
		})
	})	
	if(isUserUpdated && isCourseUpdated){
		return true;
	}else{

		return false;
	}
}