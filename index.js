const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

//Allow access to routes defined within our application
const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes')
const productsRoutes = require('./routes/productsRoutes')

const app = express();

//Connect to our MongoDB database
mongoose.connect('mongodb+srv://admin:admin@zuitt-bootcamp.ir0ca.mongodb.net/Capstone2?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.use('/users', userRoutes);
app.use('/courses', courseRoutes);
app.use('/products', productsRoutes);


app.listen(process.env.PORT || 8080, ()=> {
	console.log(`API is now online on port ${ process.env.PORT || 8080}`)
})